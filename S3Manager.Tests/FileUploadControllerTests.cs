using System;
using Xunit;
using Moq;
using S3Manager.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using S3Manager.Api.Controllers;
using System.Linq;

namespace S3Manager.Tests
{
    public class FileUploadControllerTests
    
    {
        [Fact]
        public async Task IndexReturnsAllResults()
        {
            int limit = 10;
            var mockRepo = new Mock<IUploadRequestRepository>();
            mockRepo.Setup(repo => repo.GetPendingRequests(limit))
                    .Returns(Task.FromResult(GetTestUploadRequests()));
            var controller = new FileUploadController(mockRepo.Object);

            var result = await controller.Get();

            Assert.Equal(1, result.ToArray()[0].Id);
        }

        public ICollection<UploadRequest> GetTestUploadRequests()
        {
            var requests = new List<UploadRequest>();
            requests.Add(new UploadRequest()
            {
                Id = 1,
            });

            return requests;
        }
    }
}
