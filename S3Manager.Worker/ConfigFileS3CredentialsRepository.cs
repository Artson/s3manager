﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using S3Manager.Core;

namespace S3Manager.Worker
{
    class ConfigFileS3CredentialsRepository : IS3CredentialsRepository
    {
        IConfigurationRoot config;

        public ConfigFileS3CredentialsRepository(string configFilePath)
        {
            var configurationBuilder = new ConfigurationBuilder().AddJsonFile(configFilePath);
            config = configurationBuilder.Build();

        }

        public string GetAccessKey()
        {
            return config["AWSAccessKey"];
        }

        public string GetRegion()
        {
            return config["AWSRegion"];
        }

        public string GetSecret()
        {
            return config["AWSSecretKey"];
        }
    }
}
