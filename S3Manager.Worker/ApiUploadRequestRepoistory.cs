﻿using S3Manager.Core;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace S3Manager.Worker
{
    class ApiUploadRequestRepoistory : IUploadRequestRepository
    {
        HttpClient client;

        public ApiUploadRequestRepoistory(string endPoint)
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(endPoint);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public Task CreateFileUploadRequests(ICollection<UploadRequest> requests)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<UploadRequest>> GetPendingRequests(int limit)
        {
            throw new NotImplementedException();
        }

        public async Task<ICollection<UploadRequest>> GetPendingRequestsForSource(UploadSource source, int limit)
        {
            ICollection<UploadRequest> uploadRequests = new List<UploadRequest>();

            var response = await client.GetAsync("api/FileUpload/" + source.Id);

            if (response.IsSuccessStatusCode)
            {
                uploadRequests = await response.Content.ReadAsAsync<ICollection<UploadRequest>>();
            }

            return uploadRequests;
        }

        public async Task UpdateFileUploadRequests(ICollection<UploadRequest> requests)
        {
            var response = await client.PutAsJsonAsync<ICollection<UploadRequest>>("api/FileUpload/", requests);
            response.EnsureSuccessStatusCode();
        }
    }
}
