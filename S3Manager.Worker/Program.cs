﻿using S3Manager.Core;
using System;
using System.Threading.Tasks;

namespace S3Manager.Worker
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var endpoint = "https://localhost:44362/";
            var source = new UploadSource() { Id = 1 };
            var configRepo = new ConfigFileS3CredentialsRepository("appSettings.json");
            var apiRepo = new ApiUploadRequestRepoistory(endpoint);

            var requests = await apiRepo.GetPendingRequestsForSource(source, 10);

            var worker = new UploadWorker(configRepo, apiRepo, source);
            await worker.UploadPendingRequests();
        }
    }
}
