﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S3Manager.Api.Persistance;
using S3Manager.Core;

namespace S3Manager.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileUploadController : ControllerBase
    {
        protected IUploadRequestRepository repo;

        public FileUploadController(IUploadRequestRepository repo)
        {
            this.repo = repo;
        }

        // GET: api/FileUpload
        [HttpGet]
        public async Task<ICollection<UploadRequest>> Get()
        {
            return await repo.GetPendingRequests(10);
        }

        // GET: api/FileUpload/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<ICollection<UploadRequest>> Get(int id)
        {
            return await repo.GetPendingRequestsForSource(new UploadSource() { Id = id }, 10);
        }

        // POST: api/FileUpload
        [HttpPost]
        public async Task CreateAsync([FromBody] List<UploadRequest> uploadRequests)
        {
            await repo.CreateFileUploadRequests(uploadRequests);
        }

        // PUT: api/FileUpload
        [HttpPut]
        public async Task Put( [FromBody] List<UploadRequest> uploadRequests)
        {
            await repo.UpdateFileUploadRequests(uploadRequests);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
