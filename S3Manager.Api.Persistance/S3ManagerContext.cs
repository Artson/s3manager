﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using S3Manager.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace S3Manager.Api.Persistance
{
    public class S3ManagerContext : DbContext
    {
        public virtual DbSet<UploadRequest> UploadRequests { get; set; }
        public virtual DbSet<UploadSource> UploadSources { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var configurationBuilder = new ConfigurationBuilder().AddJsonFile("databaseSettings.json");
                var configuration = configurationBuilder.Build();

                var connectionString = configuration["connectionString"];
                optionsBuilder.UseNpgsql(connectionString);
            }
        }

    }
}
