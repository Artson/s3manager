﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace S3Manager.Api.Persistance.Migrations
{
    public partial class AddUploadSource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SourceId",
                table: "UploadRequests",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UploadSources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(nullable: true),
                    IP = table.Column<string>(nullable: true),
                    Port = table.Column<int>(nullable: false),
                    LastCheckInTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadSources", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UploadRequests_SourceId",
                table: "UploadRequests",
                column: "SourceId");

            migrationBuilder.AddForeignKey(
                name: "FK_UploadRequests_UploadSources_SourceId",
                table: "UploadRequests",
                column: "SourceId",
                principalTable: "UploadSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UploadRequests_UploadSources_SourceId",
                table: "UploadRequests");

            migrationBuilder.DropTable(
                name: "UploadSources");

            migrationBuilder.DropIndex(
                name: "IX_UploadRequests_SourceId",
                table: "UploadRequests");

            migrationBuilder.DropColumn(
                name: "SourceId",
                table: "UploadRequests");
        }
    }
}
