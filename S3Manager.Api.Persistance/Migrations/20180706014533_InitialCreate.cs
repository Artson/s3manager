﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace S3Manager.Api.Persistance.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UploadRequests",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    RequestedTime = table.Column<DateTime>(nullable: false),
                    SourcePath = table.Column<string>(nullable: true),
                    DestinationBucket = table.Column<string>(nullable: true),
                    DestinationPath = table.Column<string>(nullable: true),
                    SizeInBytes = table.Column<long>(nullable: false),
                    UploadStartTime = table.Column<DateTime>(nullable: false),
                    UploadFinishTime = table.Column<DateTime>(nullable: false),
                    UploadStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadRequests", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UploadRequests");
        }
    }
}
