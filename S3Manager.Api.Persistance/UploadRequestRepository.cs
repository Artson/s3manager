﻿using S3Manager.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace S3Manager.Api.Persistance
{
    public class UploadRequestRepository : IUploadRequestRepository
    {
        private S3ManagerContext context;

        public UploadRequestRepository()
        {
            this.context = new S3ManagerContext();
        }

        public async Task<ICollection<UploadRequest>> GetPendingRequests(int limit)
        {
            var requests = await (from a in context.UploadRequests where a.UploadStatus == UploadStatus.NotUploaded select a).Take(limit).ToListAsync();
            return requests;
        }

        public async Task CreateFileUploadRequests(ICollection<UploadRequest> requests)
        {
            await context.UploadRequests.AddRangeAsync(requests);
            await context.SaveChangesAsync();
        }

        public async Task UpdateFileUploadRequests(ICollection<UploadRequest> requests)
        {
            context.UploadRequests.UpdateRange(requests);
            await context.SaveChangesAsync();
        }

        public async Task<ICollection<UploadRequest>> GetPendingRequestsForSource(UploadSource source, int limit)
        {
            var requests = await (from a in context.UploadRequests
                                  where a.UploadStatus == UploadStatus.NotUploaded && a.Source == source
                                  select a)
                                  .Take(limit)
                                  .ToListAsync();
            return requests;
        }
    }
}
