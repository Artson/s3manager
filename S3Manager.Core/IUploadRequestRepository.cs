﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S3Manager.Core
{
    public interface IUploadRequestRepository
    {
        Task<ICollection<UploadRequest>> GetPendingRequests(int limit);
        Task<ICollection<UploadRequest>> GetPendingRequestsForSource(UploadSource source, int limit);

        Task CreateFileUploadRequests(ICollection<UploadRequest> requests);

        Task UpdateFileUploadRequests(ICollection<UploadRequest> requests);
    }
}
