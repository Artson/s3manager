﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace S3Manager.Core
{
    public class UploadWorker
    {
        private readonly IS3CredentialsRepository credentialsRepo;
        private readonly IUploadRequestRepository requestRepo;
        private readonly UploadSource uploadSource;

        public UploadWorker(IS3CredentialsRepository credentialsRepo, IUploadRequestRepository requestRepo, UploadSource uploadSource)
        {
            this.credentialsRepo = credentialsRepo;
            this.requestRepo = requestRepo;
            this.uploadSource = uploadSource;
        }

        public async Task UploadPendingRequests()
        {
            var requestBatchSize = 1000;

            using (var s3Client = GetClient())
            {
                var requests = await requestRepo.GetPendingRequestsForSource(uploadSource, requestBatchSize);

                // Unlimited processing requests seem to cause errors
                var processingTaskLimit = 200;
                var processingTasks = new List<Task>();

                foreach (var request in requests)
                {
                    if (processingTasks.Count >= processingTaskLimit)
                    {
                        processingTasks.Remove(await Task.WhenAny(processingTasks));
                    }

                    processingTasks.Add(ProcessRequest(s3Client, request));
                }

                await Task.WhenAll(processingTasks);

                await requestRepo.UpdateFileUploadRequests(requests);
            }
        }

        private async Task<UploadRequest> ProcessRequest(AmazonS3Client s3Client, UploadRequest request)
        {
            try
            {
                request.UploadStartTime = DateTime.Now;
                var response = await s3Client.PutObjectAsync(ConvertRequest(request));
                request.UploadFinishTime = DateTime.Now;
                request.UploadStatus = UploadStatus.Uploaded;
            }
            catch (AmazonS3Exception exception)
            {
                if (exception.ErrorCode == "NotFound")
                {
                    request.UploadStatus = UploadStatus.Error;
                }
            }
            catch (TaskCanceledException)
            {
                request.UploadStatus = UploadStatus.Error;
            }

            return request;
        }

        private PutObjectRequest ConvertRequest(UploadRequest request)
        {
            var putRequest = new PutObjectRequest()
            {
                FilePath = request.SourcePath,
                BucketName = request.DestinationBucket,
                Key = request.DestinationPath,
            };
            return putRequest;
        }

        private AmazonS3Client GetClient()
        {
            var regionClass = RegionEndpoint.GetBySystemName(credentialsRepo.GetRegion());
            return new AmazonS3Client(credentialsRepo.GetAccessKey(), credentialsRepo.GetSecret(), new AmazonS3Config { RegionEndpoint = regionClass, DisableLogging = true });
        }
    }
}
