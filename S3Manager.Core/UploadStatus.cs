﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S3Manager.Core
{
    public enum UploadStatus
    {
        NotUploaded = 0,
        Uploaded = 1,
        Error = 2
    }
}
