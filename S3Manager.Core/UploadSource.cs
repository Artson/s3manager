﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S3Manager.Core
{
    public class UploadSource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }

        public DateTime LastCheckInTime { get; set; }

        public List<UploadRequest> UploadRequests { get; set; }
    }
}
