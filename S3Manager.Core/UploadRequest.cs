﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S3Manager.Core
{
    public class UploadRequest
    {
        public Int64 Id { get; set; }
        public DateTime RequestedTime { get; set; }
        

        public string SourcePath { get; set; }

        public string DestinationBucket { get; set; }
        public string DestinationPath { get; set; }

        public Int64 SizeInBytes { get; set; }

        public DateTime UploadStartTime { get; set; }
        public DateTime UploadFinishTime { get; set; }
        
        public UploadStatus UploadStatus { get; set; }

        public UploadSource Source { get; set; }
    }
}
