﻿using System;
using System.Collections.Generic;
using System.Text;

namespace S3Manager.Core
{
    public interface IS3CredentialsRepository
    {
        string GetRegion();
        string GetAccessKey();
        string GetSecret();
    }
}
